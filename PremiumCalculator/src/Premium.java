import java.util.ArrayList;
import java.util.List;

public class Premium {

	public static void main(String[] args) {
		Person person = new Person();
		person.setAge(55);
		person.setGender("Male");
		person.setName("Santhosh");
		double premium = calculatePremium(person);
		System.out.println("Premium="+premium);
		
	}
	public static double calculatePremium(Person person)
	{
		double premium = 5000;
		
		/*//Base premium for anyone below the age of 18 years = Rs. 5,000
		if(person.getAge()<18)
		{
			premium=premium;
		}*/
		//increase based on age: 18-25 -> + 10%
		if(person.getAge()>18 && person.getAge()<25)
		{
			premium=premium*(1+10/100);
		}
		// 25-30 -> +10% 
		if(person.getAge()>25 && person.getAge()<30)
		{
			premium=premium*(1+10/100);
		}
		// 30-35 -> +10%
		if(person.getAge()>30 && person.getAge()<35)
		{
			premium=premium*(1+10/100);
		}		
		// 30-35 -> +10%
		if(person.getAge()>35 && person.getAge()<40)
		{
			premium=premium*(1+10/100);
		}
		// 30-35 -> +10%
		if(person.getAge()>40)
		{
			int count = person.getAge()-40;
			while(count>0)
			{
				premium=premium*(1+20/100);
				count=count-5;
			}
			
		}
		if(person.getGender().equals("Male"))
		{
			System.out.println("Gender="+person.getGender());
			premium=premium*(1+2/100);
		}
		
		/*for (CurrentHealth currentHealthItem : person.getCurrentHealths()) {
			if(currentHealthItem.isItem()==true)
			{
				premium=premium*(1+1/100);
			}
		}
		for (Habit habit : person.getHabits()) {
			if(habit.habit==true)
			{
				premium=premium*(1+3/100);
			}
			else
			{
				premium=premium*(1-3/100);
			}
		}*/
		return premium;
	}

}

/*
 * Base premium for anyone below the age of 18 years = Rs. 5,000
% increase based on age: 18-25 -> + 10% | 25-30 -> +10% | 30-35 -> +10% | 35-40 -> +10% | 40+ -> 20% increase every 5 years
Gender rule: Male vs female vs Other % -> Increase 2% over standard slab for Males
Pre-existing conditions (Hypertension | Blook pressure | Blood sugar | Overweight) -> Increase of 1% per condition
Habits


Good habits (Daily exercise) -> Reduce 3% for every good habit
Bad habits (Smoking | Consumption of alcohol | Drugs) -> Increase 3% for every bad habit
 * */